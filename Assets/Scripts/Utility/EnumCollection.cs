using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EnumCollection
{
    public enum SpawnNumber
    {
        One,
        Two,
        Three,
        Four,
    }

    public enum State
    {
        MainMenu,
        Shooting,
        PlayerTurn,
        MonsterTurn,
        GameOver,
        Quit,
    }

    public enum EnemyType
    {
        CloakedZombie,
    }
}

